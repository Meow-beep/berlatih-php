<?php

    function ubah_huruf($change)
    {
        //kode di sini
        for ($i=0; $i<strlen($change); $i++) 
        { 
            $change[$i] = chr(ord($change[$i])+1);
        }
        return "$change<br>";
    }

    // TEST CASES
    echo ubah_huruf('wow'); // xpx
    echo ubah_huruf('developer'); // efwfmpqfs
    echo ubah_huruf('laravel'); // mbsbwfm
    echo ubah_huruf('keren'); // lfsfo
    echo ubah_huruf('semangat'); // tfnbohbu

?>
