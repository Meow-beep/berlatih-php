<?php

    function tukar_besar_kecil($huruf)
    {
        //kode di sini
        for ($i=0; $i<strlen($huruf); $i++) 
        { 
            $asciiCode = ord($huruf[$i]);
             
            if ($asciiCode >= 65 && $asciiCode <=90)
            {
                $huruf[$i] = chr($asciiCode+32);
            }
            elseif ($asciiCode >= 97 && $asciiCode <=122)
            {
                $huruf[$i] = chr($asciiCode-32);
            }
        }
        return "$huruf<br>";
    }

    // TEST CASES
    echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
    echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
    echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
    echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
    echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>
